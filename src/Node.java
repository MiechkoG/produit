public class Node {
    public Node leftChild, rightChild;
    public char gamma;
    
    public Node (char data, Node leftChild, Node rightChild) {
        this.leftChild = leftChild;
        this.rightChild = rightChild;
        this.gamma = data;
    }
    
    public Node (char data) {
        this.leftChild = null;
        this.rightChild = null;
        this.gamma = data;
    }
}
