import java.util.Stack;

public class Mystere {
    private Node laChose;
    private StringBuilder resultat;
    
    public Mystere (Node uneChose) {
        this.laChose = uneChose;
    }
    
    public String magie () {
        resultat = new StringBuilder();
        final Node endNodeMark = new Node(')');
        Stack<Node> nodeIncomplete = new Stack<>();
        
        Node currentNode = laChose;
        do {
            if (isNull(currentNode)) {
                addOperator(currentNode.gamma);
       
                do {
                    if (nodeIncomplete.isEmpty()){
                    	return resultat.toString();
                    }
                    currentNode = nodeIncomplete.pop();
                    addDigit(currentNode.gamma);
                } while (currentNode == endNodeMark);
                
                currentNode = currentNode.rightChild;
            }
            else {
                resultat.append('(');
                nodeIncomplete.push(endNodeMark);
                nodeIncomplete.push(currentNode);
                currentNode = currentNode.leftChild;
            }
        } while (currentNode != null);
        
        throw new RuntimeException("Le truc a imprimer est incomplet.");
    }
    
    private boolean isNull (Node c) {
        return c.leftChild == null && c.rightChild == null;
    }
    
    private void addOperator (char c) {
        if (Character.isDigit(c))
            resultat.append(c);
        else
            throw new RuntimeException("La chose " + c + " n'est pas un chiffre.");
    }
    
    private void addDigit (char c) {
        if (!Character.isDigit(c))
            resultat.append(c);
        else
            throw new RuntimeException("La chose " + c + " est un chiffre.");
    }
}
