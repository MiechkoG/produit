import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;

public class TestMystere {
    private Node[] terminaux;
    private Node petit, moyen, grand;
    
    @Before
    public void genererExemples () {
        genererTerminaux();
        
        // (1+2)
        petit = new Node('+', terminaux[1], terminaux[2]);
        
        // ((1+2)*3)
        moyen = new Node('*', petit, terminaux[3]);
        
        // ((4-5)*((1+2)*3))
        grand = new Node('*', new Node('-', terminaux[4], terminaux[5]), moyen);
    }
    
    private void genererTerminaux () {
        terminaux = new Node[6];
        terminaux[0] = new Node('+'); // Chose invalide
        for (byte i = 1; i <= 5; i++)
            terminaux[i] = new Node(Character.forDigit(i, 10));
    }
    
    @Test
    public void miniMystere () {
        Mystere m = new Mystere(terminaux[1]);
        assertEquals("1", m.magie());
    }
    
    @Test (expected = RuntimeException.class)
    public void miniMystereIncomplet () {
        Mystere m = new Mystere(null);
        m.magie();
    }    

    @Test (expected = RuntimeException.class)
    public void miniMystereInvalide () {
        Mystere m = new Mystere(terminaux[0]);
        m.magie();
    }
    
    @Test
    public void petitMystere () {
        Mystere m = new Mystere(petit);
        assertEquals("(1+2)", m.magie());
    }
    
    @Test (expected = RuntimeException.class)
    public void petitMystereIncomplet () {
        petit.rightChild = null; // (1+null)
        Mystere m = new Mystere(petit);
        m.magie();
    }

    @Test (expected = RuntimeException.class)
    public void petitMystereInvalide1 () {
        terminaux[2].gamma = '*'; // (1+*)
        Mystere m = new Mystere(petit);
        m.magie();
    }
    
    @Test (expected = RuntimeException.class)
    public void petitMystereInvalide2 () {
        petit.gamma = '9'; // (192)
        Mystere m = new Mystere(petit);
        m.magie();
    }
    
    @Test
    public void moyenMystere () {
        Mystere m = new Mystere(moyen);
        assertEquals("((1+2)*3)", m.magie());
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenMystereIncomplet () {
        petit.rightChild = null; // ((1+null)*3)
        Mystere m = new Mystere(moyen);
        m.magie();
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenMystereInvalide1 () {
        terminaux[2].gamma = '*'; // ((1+*)*3)
        Mystere m = new Mystere(moyen);
        m.magie();
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenMystereInvalide2 () {
        petit.gamma = '9'; // ((192)*3)
        Mystere m = new Mystere(moyen);
        m.magie();
    }
    
    @Test (timeout = 5)
    public void grandMystere () {
        Mystere m = new Mystere(grand);
        assertEquals("((4-5)*((1+2)*3))", m.magie());
    }
    
    @Test (timeout = 5)
    public void megaMystere () {
        petit.rightChild = new Node('*', new Node('6'), new Node('7'));
        moyen.rightChild = new Node('-', new Node('8'), new Node('9'));
        grand.leftChild.leftChild = new Node('*', new Node('2'), new Node('3'));
        Mystere m = new Mystere(grand);
        assertEquals("(((2*3)-5)*((1+(6*7))*(8-9)))", m.magie());
    }
}
